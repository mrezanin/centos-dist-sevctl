Name:           sevctl
Version:        0.1.0
Release:        3%{?dist}
Summary:        Administrative utility for AMD SEV

License:        ASL 2.0
URL:            https://github.com/enarx/sevctl
Source0:        %{url}/archive/v%{version}/%{name}-%{version}.tar.gz
Source1:        %{name}-%{version}-vendor.tar.gz

Patch0:         0001-patch-revendor-for-openssl.patch

ExclusiveArch:  %{rust_arches}
BuildRequires:  rust-toolset
BuildRequires:  openssl-devel

%description
%{summary}.


%prep
%setup -q -n %{name}-%{version}
%patch0 -p1

%cargo_prep -V 1


%build
%cargo_build


%install
%cargo_install


%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}


%changelog
* Thu Aug 26 2021 Connor Kuehl <ckuehl@redhat.com> - 0.1.0-3
- No-change rebuild to pick up gating.yaml

* Thu Aug 19 2021 Connor Kuehl <ckuehl@redhat.com> - 0.1.0-2
- Re-vendor for OpenSSL 3 compatible rust-openssl package

* Wed Apr 14 2021 Connor Kuehl <ckuehl@redhat.com>
- Initial package
